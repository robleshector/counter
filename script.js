// === Set Elements ===
let count = document.querySelector("#count");
let parityContainer = document.querySelector(".parity-container");
let parity = document.querySelector("#parity");

const increase = document.querySelector("#increase");
const decrease = document.querySelector("#decrease");
const reset = document.querySelector("#reset");


// === Set Variables ===
let value = 0;
let message;


// === Functions ===
const processValue = function() {
    // Determine if value is odd or even and update message
    if(value % 2 === 0) {
        parityContainer.classList.remove("red")
        parityContainer.classList.add("green")
        message = "The number is even";
    } else {
        message = "The number is odd";
        parityContainer.classList.remove("green")
        parityContainer.classList.add("red")
    }
    count.innerHTML = value;
    parity.innerHTML = message;
}

const increaseValue = function() {
    value++;
    processValue();
}

const decreaseValue = function() {
    value--;
    processValue();
}

const resetValue = function() {
    value = 0;
    processValue();
}


// === Event Listeners ===
increase.addEventListener("click", increaseValue, false);
decrease.addEventListener("click", decreaseValue, false);
reset.addEventListener("click", resetValue, false);


// === Initialize ===
processValue();